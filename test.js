const operations = require('./operations.js');

const assert = require('assert');

describe('testing', () => {
	it('should be 40 when you add 20 and 20', () => {
		const wantedResult = 40;
		const result = operations.add(20,20);
		assert.equal(result, wantedResult);
	})
})
